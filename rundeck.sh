==============
Rundeck
==============

========================================================
==============  Rundeck Failure Detection ============== 
#!/usr/bin/env bash
# Rundeck API token, generate under Admin > Profile
export PGSERVICE=rundeck
. "$HOME/global/vars.sh"

LIMIT=3
PROJECT_NAMES_FMT='.[].name|select(contains("-dev")|not)'
JOB_FMT='.executions[] | "Project \(.job.project) JobName: \(.job.name) Url: \(.job.permalink)"'
declare -a FAILED

function rd_curl() {
    local slug=$1
    curl --silent \
        -H "Accept: application/json" \
        -H "X-RunDeck-Auth-Token: $GS_API_KEY" \
        "http://localhost:4440/api/17/$slug"
}


for project in $(rd_curl projects | jq -r "$PROJECT_NAMES_FMT");
do
    failed_filter="project/$project/executions?statusFilter=failed&recentFilter=1h";
    # | awk -v limit=$LIMIT '$1 > $limit'
    failures=$(rd_curl "$failed_filter" | jq "$JOB_FMT" | uniq -c | awk -v limit=$LIMIT '$1 > limit');
    if [ ! -z "$failures" ];
    then
        FAILED+=("${failures}")
    fi
done

if ((${#FAILED[*]} == 0));
then
    echo "No project failures exceding $LIMIT";
    exit 0
fi
printf "Jobs that have failed with high execution failure \n %s\n" "${FAILED[*]}"
exit 1
==============


========================================================
==============  Rundeck Failure Detection ==============

# Rundeck API token, generate under Admin > Profile
export PGSERVICE=rundeck
source "$HOME/global/vars.sh"

# Update BLACK_LIST to exclude jobs
# EXCLUDE selected global jobs
BLACK_LIST="logger\|create-global-vars\|Executions\|multi-agency"
# EXCLUDE infrequent project jobs
BLACK_LIST="${BLACK_LIST}\|create-project-vars\|manual"
# EXCLUDE infrequent agency jobs
BLACK_LIST="${BLACK_LIST}\|reports/daily/VentraDailySales\|monthly\|catalogs"
echo "Job List Filter: ${BLACK_LIST}"

# Get Job Listing:
  psql -t -c "select report_id,count(*) as cnt from base_report group by report_id;" rundeck > RunDeckExecutions.txt
  grep -v "${BLACK_LIST}" "RunDeckExecutions.txt" > "FilteredRunDeckExecutions.txt"
  echo "Job list from file after filtering:"
  job_list=`awk 'BEGIN{FS="|"} {gsub(/^[[:space:]]+|[[:space:]]+$/,"");print $1 }' FilteredRunDeckExecutions.txt`
  echo $job_list
  cat FilteredRunDeckExecutions.txt | awk -F'|' '{sum+=$2}END {print"Total Executions: ";print sum;}'
  # Deletion count per request
  count=20
  # Number of requests per job, aggregates all agencies
  # This should be pretty HIGH to keep up with the jobs per day.
  repeat=2000
  deletion_count=0
  failure_count=0
  sofar=0
  stale_date=`date -d 'now - 2 months' +'%Y-%m-%d %H:%M:00'`
  # Turn array into comma delimited data
  function joiner { local IFS="$1"; shift; echo "$*"; }
  # Set field separator to New Line (to handle whitespace in job names)
  IFS=$(echo -en " ")
  echo "$job_list" | ( while read job; do
      echo ""
      echo "*** Current job: $job"
      echo "`date` - Deleting executions for job $job" >> deleted_executions.log
      echo "Querying for jobs older than $stale_date to delete ($count per request):"
      while [ $sofar -lt $repeat ];do
        # echo "Assigning list=psql -t -c \"select jc_exec_id from base_report where report_id like '$job%' and date_started <= '$stale_date' order by date_started limit $count;\" rundeck"
        list=`psql -t -c "select jc_exec_id from base_report where report_id like '$job%' and date_started <= '$stale_date' order by date_started limit $count;" rundeck`
        if [ -n "$list" ];then
            ((sofar+=1))
            echo "Deleting..."
            echo ${list} >> deleted_executions.log
            idset=`joiner , ${list}`
            response=`curl -w "\n" --silent -X POST http://localhost:4440/api/12/executions/delete -d '{"ids": ['$idset']}' -H "Content-Type: application/json" -H "X-RunDeck-Auth-Token: $GS_API_KEY" --connect-timeout 60 -m 60`
            echo "Response: $response"
            ((deletion_count+=`echo $response | jq .successCount`))
            ((failure_count+=`echo $response | jq .failedCount`))
        else
            echo "...none found."
            sofar=$repeat
        fi
      done
      sofar=0
    done && printf "\nJob completed, deleted ${deletion_count} record(s), failed to delete ${failure_count} record(s).")


========================================================
================  Create Backoff Script ================
#!/bin/bash
file=$HOME/global/with_backoff.sh

if [ ! -d "$(dirname $file)" ]; then
  echo "create directory $(dirname $file)"
  mkdir -p $(dirname $file)
fi

cat > $file << "EOF"
#!/bin/bash
# Created by Rundeck $(date)
# From: https://coderwall.com/p/--eiqg/exponential-backoff-in-bash
# source this file to gain access to with_backoff
# Retries a command a configurable number of times with backoff.  Successive
# failures double the timeout.
#
# Environment variables:
# BACKOFF_ATTEMPTS The max number of tries (default 8)
# BACKOFF_TIMEOUT The timeout in seconds (default 4)
function with_backoff {
local max_attempts=${BACKOFF_ATTEMPTS-8}
local timeout=${BACKOFF_TIMEOUT-4}
local attempt=0
local exitCode=0
local initialCommand="$@"
local wrappedCommand="${initialCommand}"

while (( attempt < max_attempts ))
do
  set +e
  ${wrappedCommand}
  exitCode=$?
  set -e

  if [[ $exitCode == 0 ]]
  then
    break
  fi

  echo "Failure (${exitCode})! Retrying in $timeout.." 1>&2
  sleep $timeout

  attempt=$(( attempt + 1 ))
  timeout=$(( timeout * 2 ))

  if [[ attempt < max_attempts ]]; then
    newIP=$(getJobInstance | jq -r '.PrivateIpAddress')
    # Replaces the IP in the "gs-rundeck@222.212.222.111 cd /opt/gs/" portion of the command
    wrappedCommand=${wrappedCommand/\@*cd \/opt\/gs\//@$newIP cd \/opt\/gs\/}
    # Replaces the IP in "gs-rundeck@172.18.16.24 python /opt/gs"
    wrappedCommand=${wrappedCommand/rundeck\@*python \/opt\/gs/rundeck@$newIP python \/opt\/gs}
    echo "Will retry with ${newIP}"
    echo "New command:"
    echo "${wrappedCommand}"
  fi

done

if [[ $exitCode != 0 ]]
then
  echo "The following command failed after ${max_attempts} attempts:"
  echo ">> ($@) " 1>&2
fi

return $exitCode
}

if [ $# -ne 0 ]; then
  with_backoff "$@"
fi

EOF

chmod +x $file

echo
echo created $file
echo
echo -----------------
cat $file
