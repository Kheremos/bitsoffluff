#!/usr/bin/python

import boto3
""" Import botocore for Error handling """
import botocore
from subprocess import check_output
import json
import datetime
import time
import os

sqs = boto3.resource('sqs')
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')
ec2 = boto3.resource('ec2')

DEBUG = True
SANDBOX = False
VERBOSE = 2
# Maestro will delete an older file if greater than this (soft)
DIST_THRESHOLD = 7
# Maestro wait time in minutes.
MAESTRO_WAIT = 3
fileName = None
notifyTeamTemplate = "notificationTemplate.sh"
notifyTeamScript = "sendNotifications.sh"
jenkinsBucket = "gts-jenkins"
infrastructureQueue = "Infrastructure-Maestro-Queue"
processedBuilds = {}

""" Helper method for conditional logging (ex: DEBUG and VERBOSE) """
def logger(condition, string):
    if condition:
        print(string)

    return

""" JSON Pretty Printing """
def prettyPrint(condition, labelString, stringToPretty):
    if not condition:
        return

    parsed = json.loads(stringToPretty)
    print(labelString.format(json.dumps(parsed, indent=3)))

    return

""" Takes a string representation of an S3 build object and returns
    the build number as an int. """
def extractBuildNum(element):
    try:
        return int(element.split("/")[2])

    except:
        return 0


"""Given a tagable ec2_object, return dictionary of existing tags."""
def make_tag_dict(ec2_object):
    tag_dict = {}
    if ec2_object.tags is None:
        return tag_dict

    for tag in ec2_object.tags:
        tag_dict[tag['Key']] = tag['Value']

    return tag_dict


def main():
    fileName = getFileNameFromQueue(infrastructureQueue)
    if not fileName:
        print("!!!\tfileName not assigned.")
        return

    if fileName == "Empty":
        print("INFO: Queue appears empty.")
        return

    latestFile, resultSet = getLatestFile(fileName)
    if not latestFile:
        print("!!!\tFailed to find latest build file similar to %s" % fileName)
        return

    if alreadyProcessed(latestFile):
        logger(VERBOSE, "INFO:\t%s has already been processed.\n\tNo further action required" % latestFile)
        return

    trimS3Queue(resultSet)

    metaTags = getMetaTags(latestFile)
    if not metaTags:
        print("!!!\tMetaData tags not found.")
        return

    logger(VERBOSE > 2, "INFO: Found metaData: %s" % metaTags)
    localFile = downloadFile(latestFile)
    if not localFile:
        print("!!!\tFailed to download %s from s3." % fileName)
        return

    instances = getInstances(metaTags['buildinfo'])
    if len(instances) == 0:
        print("INFO: No instances matching %s" % metaTags['buildinfo'])
        return

    updateNotifyTeamScript(instances)
    for instance in instances:
        ipAddress = instance.public_ip_address
        logger(VERBOSE > 1, "About to access %s @ %s" % (instance, ipAddress))
        # print check_output("./sendToInstance.sh")
        # scp -P 3822 -i /home/ubuntu/.ssh/LXWebDevKeyPair.pem $1 ubuntu@$2:/home/ubuntu/
        output = check_output(["./sendToInstance.sh %s %s" % (localFile, ipAddress)], shell=True)
        logger(VERBOSE, "INFO: Sending files (%s %s) to instance@%s - %s" % (localFile, notifyTeamScript, instance.public_ip_address, output))
        output = check_output(["./sendToInstance.sh %s %s" % (notifyTeamScript, ipAddress)], shell=True)
        logger(VERBOSE, output)
    """ Delete the SQS message here? """

    return


def getFileNameFromQueue(queueName):
    queue = sqs.get_queue_by_name(QueueName=queueName)
    prettyPrint(VERBOSE > 4, "INFO: Queue Attributes\n{0}\n", str(queue.attributes).replace("\"", "?@?").replace("\'", "\"").replace("?@?", "\'"))
    logger(VERBOSE > 2, "INFO: Queue Url:\n%s\n" % (queue.url))

    for message in queue.receive_messages(AttributeNames=['All']):
        """ Get the custom author message attribute if it was set """
        if message.attributes is not None:
            prettyPrint(VERBOSE > 4, "INFO: Message: {0}", str(message.attributes))

        try:
            fileName = json.loads(message.body)['Records'][0]['s3']['object']['key']
            logger(VERBOSE > 2, "INFO: Found message regarding %s" % fileName)
            try:
                receiveCount = int(message.attributes.get('ApproximateReceiveCount'))
                if receiveCount > 20:
                    logger(VERBOSE > 2, "INFO: Deleting a message from SQS (seen around %s times)" % receiveCount)
                    message.delete()

            except AttributeError as e:  # 'NoneType' object has no attribute 'get'
                print("!!!\tError when retrieving receiveCount for SQS message: %s" % e)
            return fileName

        except KeyError as e:
            print("!!!\tError when retrieving object key: %s" % e)
            return None

    return "Empty"


def getLatestFile(fileName):
    """ Gets the latest build file with the same prefix (same job) of fileName
    Also returns the resultSet, so that the files stored on S3 can be cleaned
    during this process.
    """
    jobDirectory = "/".join(fileName.split("/", 2)[0:2])
    logger(VERBOSE > 3, "INFO: Filtering bucket results by prefix: %s" % jobDirectory)
    resultSet = []

    """ TODO: Add try/except block? """
    for buckObj in s3.Bucket(jenkinsBucket).objects.filter(Prefix=jobDirectory):
        resultSet.append(buckObj.key)
        logger(VERBOSE > 3, "INFO: getLatestFile() - Adding %s to result set" % buckObj.key)

    resultSet.sort(key=extractBuildNum)
    latestResult = resultSet.pop()
    logger(VERBOSE > 3, "INFO: getLatestFile() - Returning %s" % latestResult)
    return (latestResult, resultSet)


def alreadyProcessed(fileName):
    """ Checks if fileName has already been processed, allowing Maestro
    to avoid unnecessary work. """
    """ Example tokens: [u'jobs', u'TeslaApp-D', u'334', u'dist.tgz'] """
    fileTokens = fileName.split("/")
    buildJob = fileTokens[1]
    buildNumber = fileTokens[2]
    if processedBuilds.get(buildJob, 0) < buildNumber:
        processedBuilds[buildJob] = buildNumber
        return False

    return True


def trimS3Queue(s3fileList):
    """ If the S3 directory for a job has more than DIST_THRESHOLD, this
    function will delete the oldest SINGLE file. Expects sorted list.
     Reverses and pops for deletion request.
    """
    if (len(s3fileList) <= DIST_THRESHOLD):
        return

    jBucket = s3.Bucket(jenkinsBucket)
    s3fileList.reverse()
    resultToDelete = s3fileList.pop()
    response = jBucket.delete_objects(
        Delete={
                'Objects': [{'Key': resultToDelete}],
                'Quiet': False
               })
    logger(VERBOSE > 1, "INFO:\tAttempted to delete %s from s3.\n\tResponse was: %s" % (resultToDelete, response))


def getMetaTags(fileToFind):
    print("Retrieving MetaTags for %s" % fileToFind)
    foundObject = s3.Object(jenkinsBucket, fileToFind)

    try:
        objectTwo = foundObject.get()
        print("***\tFound: %s " % foundObject)
        return foundObject.metadata

    except botocore.exceptions.ClientError as e:
        print("!!!\tError when retrieving %s\n!!!\t%s" % (fileToFind, e))
        return None


def downloadFile(fileToFind):
    """ The boto3 API will freeze if the directory isn't pre-existing, so we save in folder """
    nameToAssign = "dist.tgz"
    logger(VERBOSE > 0, "INFO: Accessing %s to download %s and assign to %s" %
           (jenkinsBucket, fileToFind, nameToAssign))

    try:
        if not SANDBOX:
            s3_client.download_file(jenkinsBucket, fileToFind, nameToAssign)

        return nameToAssign

    except botocore.exceptions.ClientError as e:
        print("!!!\t%s" % e)
        return None


def getInstances(buildInfo):
    noBuildInfo = []
    retInstances = []
    logger(VERBOSE > 1, "INFO: Searching instances for %s" % buildInfo)
    instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

    for instance in instances:
        tags = make_tag_dict(instance)
        logger(VERBOSE > 3, "INFO: tags - %s" % tags)
        if tags.get('BuildInfo'):
            logger(VERBOSE > 3, "\tINFO:Checking %s..." % tags.get('Name'))
            if tags.get('BuildInfo') == buildInfo:
                logger(VERBOSE > 1, "\tINFO:\tMatched buildInfo for %s (%s)" % (tags.get('Name'), instance.id))
                retInstances.append(instance)
        else:
            noBuildInfo.append(tags.get('Name'))

    if (len(noBuildInfo) > 0):
        logger(VERBOSE > 2, "INFO: The following instances have no buildinfo:\n\t%s" % noBuildInfo)

    logger(VERBOSE > 1, "INFO: Returning instance(s) %s" % retInstances)
    return retInstances


def updateNotifyTeamScript(instances):
    contactList = set()
    for instance in instances:
        tags = make_tag_dict(instance)
        for contact in ['Lead Developer', 'lead developer', 'Developer', 'developer']:
            if tags.get(contact):
                contactList.add(tags.get(contact))
    stringList = ""
    logger(VERBOSE > 4, "\tINFO: contactList - %s " % contactList)
    for elem in contactList:
        stringList += "%s " % elem

    logger(VERBOSE > 4, "\tINFO: stringList - %s " % stringList)
    output = check_output(["sed 's/PEOPLE_TO_INFORM/%s/' %s > %s" %
                          (stringList, notifyTeamTemplate, notifyTeamScript)],
                          shell=True)
    check_output(["chmod +x %s" % (notifyTeamScript)], shell=True)


def loopMain():
    print "%s Starting main() loop" % \
          (time.strftime("%d %b %H:%M:%S", time.localtime()))
    while True:
        time.sleep(3)
        main()
        print ("%s Looping main() every %s minutes..." % (time.strftime
               ("%d %b %H:%M:%S", time.localtime()), MAESTRO_WAIT))
        time.sleep(MAESTRO_WAIT * 60)


os.environ['TZ'] = 'US/Pacific'
time.tzset()
loopMain()
# main()
