// Edited: 11/10/2015-GG -Changed PR comment on build page to link.
// ********** LOGGING ************** // Could not merge
def DEBUG = false;
def logger = { logThis, msg -> logThis ? manager.listener.logger.println(msg) : null }
logger(true, "*** Groovy Postbuild Script ***")
logger(DEBUG, " *** DEBUGGING ENABLED")
 
// ********** SYMBOLOGY ************** //
/** ✔ 🔎 🔍 🔆 ☃ 🔋 🚷 🔛 💈  👷 👃 🐱 🔙 ⌦ ⌧ 🐽 🚗 🗻 ↂ
*** 📈 ❀ 🐭 ⍰ 📉 🔪 🔥 🔜 🐬 🔓 🔒 🔧 💘 ➘ 👭
*** 📷 ✆ 🔄 👀 🍣 💖 ⛾ 🕔 💫 ✉ ✎ **/

def COV_GOOD =  "✔" 
def COV_BAD = "📉"
def VIO_GOOD = "🐬"
def VIO_BAD = "🐼"
 
// ********** ENVIRONMENT ************** //
def BUILD_NUMBER = manager.getEnvVariable('BUILD_NUMBER')
def JOB_NAME = manager.getEnvVariable('JOB_NAME')
def USER = manager.getEnvVariable('JENKINS_SECRET')
def REPO_KEY = manager.getEnvVariable('PULL_REQUEST_FROM_REPO_PROJECT_KEY')
def REPO_SLUG = manager.getEnvVariable('PULL_REQUEST_FROM_REPO_SLUG')
def PR_ID = manager.getEnvVariable('PULL_REQUEST_ID')

// ********** LINKS ************** //
def buildLink = "http://jenkins.gtsservices.com/job/${JOB_NAME}/${BUILD_NUMBER}/"
def commentURI =  "https://stash.gtsservices.com:8443/rest/api/1.0/projects/${REPO_KEY}/repos/${REPO_SLUG}/pull-requests/${PR_ID}/comments"
def jenkinsVioReport = "http://jenkins.gtsservices.com/job/${JOB_NAME}/${BUILD_NUMBER}/violations"
def coverageReportURL = "http://jenkins.gtsservices.com/job/${JOB_NAME}/${BUILD_NUMBER}/cobertura"
 
// ********** SET/UPDATE BUILD COMMENT ************** //
def buildComment= { "[${manager.getResult()} for BUILD ${JOB_NAME}-${BUILD_NUMBER} ](${buildLink})" }
 
// ********** METRICS ************** //
// desired coverage
def COV = 70
// max violations
int VIO = 10
int actionCount = 0
int jslintViolations
 
// ********** NOTIFY IF CONFIGURATION FAILURE ************** //
logger((USER==null)," !!! NO STASH-JENKINS CREDENTIALS FOUND !!!")
logger(DEBUG, " *** Variables configured, checking build actions:")
manager.build.actions.each { action ->
    if(action.class.simpleName == "ViolationsBuildAction") {
       jslintViolations = action.report.violations.get("jslint") ?: 0
    }
    actionCount++
    manager.listener.logger.println("manager build action name: "+action.class.simpleName);
}
 
// *********************  GET COVERAGE AND ADD TO COMMENT ****************** //
def coverage = "No Coverage Found!!"
def PRComment  = ""
def clientCovLine= ""
def violationComment = ""
// Line coverage regex, looks for: "Lines        : 91.19% ( 435/477 )"
def lineCountRegex =".*Lines.*?([0-9]+\\.?[0-9]+).*"
def branchCovRegex=".*Branches.*?([0-9]+)\\.?[0-9]+.*"
def karmaCovRegex="(.*All files.*?([0-9]+\\.?[0-9]+).*\\|.*)"

// Problem is with the karmaCovRegex.  Compare to the one above it?  What's the deal?
def lineCov = 0
def branchCov = 0
def karmaLineCov = 0
def karmaBranchCov = 0
def vioSymbol = VIO_GOOD

/** Look for LINE coverage in build log**/
def matcher = manager.getLogMatcher(lineCountRegex)
logger(DEBUG, " *** Checking matcher..."+(matcher)+"  -  "+(matcher?.matches()))
if (matcher?.matches() == null ) {
    logger(DEBUG," *** No LINE coverage found in build log.")
} else {
    manager.addShortText(""+matcher.group(1).toFloat().toInteger(), "grey", "white", "0px", "white")
    coverage = "Line Coverage: "+matcher.group(1)
    lineCov = matcher.group(1)
}

/** Look for BRANCH coverage in build log **/
matcher = manager.getLogMatcher(branchCovRegex)
if (matcher?.matches() == null ) {
    logger(DEBUG," *** No BRANCH coverage found in build log.")
} else {
    coverage += "  Branch Coverage: "+matcher.group(1)
    branchCov = matcher.group(1)
}

/** Look for KARMA coverage in build log **/
matcher = manager.getLogMatcher(karmaCovRegex)
if (matcher?.matches() == null ) {
    logger(DEBUG," *** No KARMA matches found in build log.")
} else {
    def covArray = matcher.group(1).split(/\|/)
    karmaBranchCov = covArray[2].trim()+"%"
    karmaLineCov = covArray[4].trim()+"%"
    clientCovLine= " [Client]($coverageReportURL)  | ${karmaLineCov } | ${karmaBranchCov} "
}

/** Check for failing tests **/ 
matcher = manager.getLogMatcher(".*([0-9]+) failing.*\$")
if(matcher?.matches()) {
    PRComment+=matcher.group(1)+" tests failing.  "    
}

/** Check Violations **/
if ( VIO.toInteger() <= jslintViolations .toInteger()   ) {
 vioSymbol = VIO_BAD
 manager.buildUnstable()
} 
if (jslintViolations.toInteger() > 0 ){
  def vioString = "[${jslintViolations} ${vioSymbol} ](${jenkinsVioReport})"
  PRComment += "Violations detected: ${vioString}\\n"
}

/** Check for Merge Errors **/
if(manager.logContains(".*Branch not suitable for integration as it does not merge cleanly.*")) {
    manager.addWarningBadge("Unable to merge branch!")
    manager.createSummary("warning.gif").appendText("<h1>Unable to merge branch!</h1>", false, true, false, "red")
}

/** Check for NPM Errors **/
if(manager.logContains(".*npm ERR!.*")) {
    manager.addWarningBadge("NPM Errors Detected!")
    PRComment+="NPM Errors Detected!\\n"
    manager.createSummary("warning.gif").appendText("<h1>NPM Errors Detected!</h1>", false, true, false, "red")
    manager.buildFailure()
}


// ******* ADD PR_ID TO BUILD SUMMARY ******* //
// https://stash.gtsservices.com:8443/projects/TSL/repos/tesla-app/pull-requests/135/overview
def linkToPR = "<h3><a href='https://stash.gtsservices.com:8443/projects/$REPO_KEY/repos/$REPO_SLUG/pull-requests/$PR_ID/overview'>Pull Request $PR_ID</a></h3>"
manager.createSummary("/plugin/git/icons/git-48x48.png").appendText(linkToPR, false, false, true, "blue")
  
def covSymbol = (COV.toFloat() < lineCov.toFloat()) ? COV_GOOD:COV_BAD
manager.addShortText(vioSymbol+" "+covSymbol,  "grey", "white", "0px", "white")

// coverageReportURL
def serverLine = "[Server]($coverageReportURL)  | ${lineCov}% | ${branchCov}% "

PRComment = buildComment() + "\\n${PRComment }\\n  | Line Coverage | Branch Coverage\\n------|-------\\n${serverLine}\\n${clientCovLine}"
data =  "{ \"text\": \"${ PRComment }\" }"
if (USER && data && commentURI) {
    response = ["curl","-D-","-u",USER, "-H", "Content-Type: application/json", "-H", "Accept: application/json", "-X", "POST", "-d", data, commentURI].execute().text
    logger(DEBUG, " *** curl sent... Response: "+response)
}