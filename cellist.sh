#!/bin/bash

distFile="$HOME/dist.tgz"
distBackup="$HOME/oldDist"
indexFile="$HOME/dist/public/index.html"
packageJson="$HOME/dist/package.json"
myIp="$(dig +short myip.opendns.com @resolver1.opendns.com)"
emailSubject="Error updating latest build."

# If public directory doesn't exit...
if [ ! -f $indexFile ];
then
  indexFile="$HOME/dist/client/index.html"
fi

checkForBuildArtifact () {
    if [ -e $distFile ]
    then
       echo "Found $distFile"
       logRoll
       processDist
    fi
}

panicExit () {
  echo $1
  body=" $1
  Problem Instance:  $myIp"
  # sendNotifications.sh is created and provided by maestro, and
  # sends a notification to all Developers tagged for
  # this instance.  It arrives with a dist.tgz
  $HOME/sendNotifications.sh "$emailSubject" "$body" "$HOME/npm_install.log"
  pm2 stop all
  pm2 start "${distBackup}/dist/server/app.js" --merge-logs
  exit
}

verifyConfig() {
  if [ ! -d "$distBackup" ]; then
    echo "$distBackup folder not found, creating..."
    mkdir $distBackup
  fi
}

checkMigrations(){
    pushd .
    cd $HOME/dist
    echo "Checking for awsMigration task"
    if grep -q "awsMigrate" "package.json"; then
      npm run awsMigrate 2>> "$HOME/npm_install.log"
      panicExit "Detected 'migration task in package.json' cellist halting!"
    fi
    popd
}

logRoll() {
   logrotate -f /etc/logrotate.d/pm2rotation -s $HOME/.pm2/rotationStatus
}

processDist() {

    # This is set up to copy the directory, and then remove
    # public and server directories, leaving node_modules
    if [ -d "$HOME/dist" ]; then
      echo "Backup old dist folder to $distBackup"
      cp -r $HOME/dist/ $distBackup
      rm -rf $HOME/dist/public
      rm -rf $HOME/dist/client
      rm -rf $HOME/dist/server
    fi

    echo "Extract new dist"
    tar -C ~ -zxf $distFile

    if [ -e $packageJson ]; then
      echo "Remove NPM postInstall script from $packageJson"
      sed -i 's/postinstall\":\W*\".*\"/postinstall\": \"\"/' $packageJson
#      checkMigrations
    fi

    echo "NPM install"
    cd $HOME/dist

    # Append or replace?
    npm install 2>> "$HOME/npm_install.log"

    cd -

    echo "Check for NPM errors and exit if found"
    if grep -q "ERR!" "$HOME/npm_install.log"; then
      panicExit "Detected 'ERR!' during NPM install, monitoring halted!!!"
    fi

    if [ -e $indexFile ] ; then
     echo "Injecting $dateString into $indexFile"
     sed -i "s/<\/body>/$dateString\n<\/body>/" $indexFile
     echo "<!--" >> $indexFile
     cat $HOME/dist/distInfo >> $indexFile
     echo "-->" >> $indexFile
    fi
    pm2 stop all
    pm2 start ~/dist/server/app.js --merge-logs
    mv $distFile $distBackup
}

verifyConfig

 COUNTER=0
 while [  $COUNTER -lt 10000 ]; do
   date=`TZ=US/Pacific date` 
   dateString="<\!--$date-->"
   echo "$date: The counter is $COUNTER"
   let COUNTER=COUNTER+1
   checkForBuildArtifact
   echo "$date: Sleeping for five minutes"
   sleep 300
  done
  body="Cellist script has completed its cycles and is no longer auto-updating.
  Instance:  $myIp"
 ./sendNotifications.sh "Cellist has concluded naturally for $(dig +short myip.opendns.com @resolver1.opendns.com)" "$body" "$HOME/npm_install.log"
