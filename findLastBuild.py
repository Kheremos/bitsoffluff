#!/usr/bin/python

import boto3
import os

client = boto3.client('s3', 'us-west-2')
s3 = boto3.resource('s3')
jenkinsBucket = "gts-jenkins"
fileString = "jobs/TeslaApp-D/324/dist.tgz"
fileString = "jobs/ED_rightglass_master_g/11/dist.tgz'"
jBucket = s3.Bucket(jenkinsBucket)

print "dir(s3client): \n\t",dir(client)
print "dir(s3resource): \n\t",dir(s3)
print("s3.buckets:\n\t"+str(s3.buckets))

def extractBuildNum(element):
    """ Takes an S3 object string and provides a sortable value """
    try:
        print "element.split: %s" % element.split("/")[2]
        return int(element.split("/")[2])
    except:
        return 0

""" List various details about the bucket under test (but) """
def bucketTesting(but):
#    print("bucket: \n\t%s - %s" % (but, type(but)))
#    print("dir(%s):\n\t%s" %(but,dir(but )))
#    print("dir(%s):\n\t%s" %(but.objects,dir(but.objects )))
    prefix = "/".join(fileString.split("/",2)[0:2])
#    jobFolder = 
    resultSet = filesSimilarTo(but.objects, prefix)
    print "Filtered results:\n\t%s" % resultSet
    resultSet.sort( key=extractBuildNum )
    print "Filtered sorted results:\n\t%s" % resultSet
    resultToDelete = resultSet.pop()
    print "Filtered pop:\n\t%s" % resultToDelete
    print jBucket.delete_objects(
    Delete={
        'Objects': [
            {
                'Key': resultToDelete,
            }
        ],
        'Quiet': False
     })
    

def main():
    fileName = getFileNameFromQueue(infrastructureQueue)
    if not fileName:
        print("!!!\tfileName not assigned.")
        return

def filesSimilarTo(filterSet, filterString):
    resultSet = []
    print("Unfiltered set:")
    for buckObj in filterSet.all():
        print("\t%s" % buckObj)    
    print("Filtering for %s" % filterString)
    for buckObj in filterSet.filter(Prefix=filterString):
        print("\t%s" % buckObj)
        resultSet.append(buckObj.key)
    return resultSet  

def oldStuff():
# Print out bucket names
    for bucket in s3.buckets.all():
        print(bucket.name)
        if bucket.name == jenkinsBucket:
            print "\tFound %s bucket:" % bucket.name
            thingOfInterest = bucket.objects.all()
            print "Investigating %s" % thingOfInterest
            print "dir(%s):" % thingOfInterest
            print "\t%s" % dir(thingOfInterest)
            print "parent: %s "
            print "methods:"
            print "\t%s" % [method for method in dir(thingOfInterest) if callable(getattr(thingOfInterest, method))]
            print "Properties:"
            for property, value in vars(thingOfInterest).iteritems():
                print "\t",property, ": ", value
            #        print bucket.objects.all().batch_actions()
            for key in bucket.objects.all():
                 print key
            #            obj = s3.Object(bucket.name,key.key)
            #            response = obj.get()
            #            print "\ttags:\t%s"%key.metadata
            #            print "\t%s\t%s"%(obj.key,obj.metadata)
            #            print "\t%s"%(object.metadata.get('buildinfo'))
            #            print "\t%s"%key.metadata
            #       print "\t{name}\t{size}\t{modified}".format(
            #          name = key.name,
            #         size = key.size,
            #        modified = key.last_modified,  )i

bucketTesting(jBucket)
