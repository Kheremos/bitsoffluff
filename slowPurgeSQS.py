#!/usr/bin/python
# Not this one #!/usr/bin/env python

import boto3
import json

sqs = boto3.resource('sqs')

queue = sqs.get_queue_by_name(QueueName='Infrastructure-Maestro-Queue');

print "Url:\n%s\n" % (queue.url)
""" Replace the nexted double quotes with single quotes so they can be JSON parsed. """
parsed = json.loads(str(queue.attributes).replace("\"","?@?").replace("\'","\"").replace("?@?","\'"))
print "Attributes:\n%s\n" % json.dumps(parsed,indent=3,sort_keys=True)


""" JSON Pretty Printing
    condition: Arg that resolves to True/False
    labelString: The string containing {0}
    stringToPretty: JSON String Object
"""
def prettyPrint(condition, labelString, stringToPretty):
    if not condition:
        return

    parsed = json.loads(stringToPretty)
    print(labelString.format(json.dumps(parsed, indent=3)))

    return


# Process messages by printing out body and optional author name

#for message in queue.receive_messages(AttributeNames=['All'],MessageAttributeNames=['.*'],MaxNumberOfMessages=5):
for message in queue.receive_messages(AttributeNames=['All'],MaxNumberOfMessages=10):
    # Get the custom author message attribute if it was set
    author_text = ''
    if message.message_attributes is not None:
        print('Message.message_attributes: {0}'.format(message.message_attributes))
        author_name = message.message_attributes.get('Author').get('StringValue')
        if author_name:
            author_text = ' ({0})'.format(author_name)

    # Print out the body and author (if set)

#    prettyPrint(True,"Full Message:\n{0}\n\n",str(message))
    print("Message:\n{0}\n\n").format(message)
    print(dir(message))
    print("Message.attributes: %s" % message.attributes)
    recCount = int(message.attributes.get('ApproximateReceiveCount'))
    print("Receive Count: %s" % recCount)
    if recCount > 40:
        print("We should delete this message.")
    else:
        print("This message is still fresh!")

#    print(message.message_attributes.load('ApproximateReceiveCount'))

   # print("Message.attributes.get('ApproximateReceiveCount'):\n{0}\n").format(message.get('ApproximateReceiveCount'))

    print("Message:\n{0}\n").format(message)
    print('Message.body: {0} \n'.format(message.body))
    # Let the queue know th

# Print out each queue name, which is part of its ARN
#for queue in sqs.queues.all():
#    print(queue.url)

